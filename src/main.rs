use std::{cmp::Ordering, io};

use rand::Rng;

fn main() {
    println!("Hello world!");

    let num = rand::thread_rng().gen_range(1..100);

    println!("Try to guess a number from 1 to 100");
    let mut i = 1;
    loop {
        let mut guess = String::new();

        io::stdin().read_line(&mut guess).expect("Input error.");

        let guess: i32 = match guess.trim().parse() {
            Ok(number) => number,
            Err(_) => continue,
        };

        match guess.cmp(&num) {
            Ordering::Less => println!("Its small..."),
            Ordering::Equal => {
                println!("Prefect... You win.");
                break;
            }
            Ordering::Greater => println!("Its big..."),
        }
        i += 1;
    }
    println!("Secret number was : {}", &num);
    println!("Number of trials: {}", &i);
}
